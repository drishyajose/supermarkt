    function deleteProduct(id) {
        if (confirm("Are you sure to delete this product?")) {
            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax(
                {
                    url: "delete-product",
                    type: 'DELETE',
                    data: {
                        "id": id,
                        "_token": token,
                    },
                    success: function (data) {
                        alert('Product Deleted Successfully');
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        console.error("AJAX Request Error:", status, error);
                    }
                });
        }
    }
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    function addToCart(pid){
            // alert(pid);
            $.ajax({
              url: 'add-to-cart',
              type: 'POST',
              data: { pid: pid},
              dataType: 'JSON',
              beforeSend: function (xhr) {
              },
              success: function (res) { 
                if(res == true)
                    alert('added to cart successfully');
                else
                    alert("Failed ! Try again !");
              },
              error: function (error) {
                console.log('Request Error');
              }
           });
    }