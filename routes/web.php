<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('intro');
})->name('main');
Route::get('/success', function () {
    return view('successlanding');
});

Route::get('/products', 'App\Http\Controllers\ProductsController@view')->name('view-products');
Route::get('/get-category', 'App\Http\Controllers\ProductsController@getCategory')->name('get-category');
Route::post('/save-products', 'App\Http\Controllers\ProductsController@saveProducts')->name('save-products');
Route::delete('/delete-product', 'App\Http\Controllers\ProductsController@delete')->name('delete-product');

Route::get('/view-cart', 'App\Http\Controllers\AddToCartController@viewCart')->name('view-cart');
Route::post('/add-to-cart', 'App\Http\Controllers\AddToCartController@addToCart')->name('add-to-cart');
Route::delete('/edit-cart', 'App\Http\Controllers\AddToCartController@delete')->name('edit-cart');

Route::post('/add-one-product', 'App\Http\Controllers\AddToCartController@addProductToCart');
Route::delete('/delete-one-product', 'App\Http\Controllers\AddToCartController@deleteProductFromCart');

Route::post('/order', 'App\Http\Controllers\AddToCartController@placeOrder');


