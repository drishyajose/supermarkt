<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\ProductsRepository;
use App\Services\ErrorLog;



class ProductsBulkInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $productsRepo;

    /**
     * Create a new job instance.
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->productsRepo = new ProductsRepository();
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        if(isset($this->data) && !empty($this->data))
        {
            try{
                $dataArr = $this->data;
                foreach ($dataArr as $key => $value) {            
                    $message =  $this->productsRepo->saveProducts($value);
                }
            } catch (Exception $e) {
                ErrorLog::log($e->getMessage(),'error', __METHOD__);
            }
        }
    }
}
