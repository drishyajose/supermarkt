<?php

namespace App\Services;
use Log;

class ErrorLog
{

    //sample call : ErrorLog::log($e->getMessage(), 'error', __METHOD__);
    public static function log($message,$type, $method = null)
    {
        $message = $message;
        if($method)
            $message .= '--Class: '.$method;

        Log::$type($message);
    }
}