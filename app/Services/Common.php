<?php

namespace App\Services;
use Log;
use DB;
use App\Services\ErrorLog;

class Common
{

    public static function checkFieldExists($table = null,  $field = null, $id = null)
    { 
        try {
            if (!$id || !$table || !$field)
                return false;
            return $count = DB::table($table)->where($field, $id)->count();
        } catch (\Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);
            return false;
        }
    }
}