<?php

namespace App\Repositories;

use App\Services\ErrorLog;
use Exception;
use App\Models\Products;
use App\Models\Categories;
use App\Jobs\ProductsBulkInsert;
use App\Services\Common;

class ProductsRepository {
    
    public function __construct()
    
    {
       
    }
    public function listProducts($data)
    {
            // $products = Products::with('category')->get()->toArray();
            $name  = isset($data['proname']) ? $data['proname'] : null;
            $code  = isset($data['procode']) ? $data['procode'] : null;
            $price = isset($data['proprice']) ? number_format((float)$data['proprice'], 2, '.', '') : null;
            $category  = isset($data['category']) ? $data['category'] : null;
            // dd($price);
            $products = Products::with('category');
            if($name)
                $products->where(function ($query) use ($name) {  $query->orWhere('products.name', 'LIKE', '%' . $name . '%'); });
            if($code)
                $products->where(function ($query) use ($code) {  $query->orWhere('products.code',$code); });
            if($price)
                $products->where(function ($query) use ($price) {  $query->orWhere('products.price',$price); });
            if($category)
                $products->where(function ($query) use ($category) {  $query->orWhere('products.category_id',$category); });
            $products = $products->paginate(5);
            return $products;
            // dd($products);
    }
    public function processCsv($file)
    {
        try {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileSize = $file->getSize();
                $mimeType = $file->getMimeType();
                $valid_extension = array("csv",);
                $maxFileSize = 2097152;//2MB 
                 if(in_array(strtolower($extension),$valid_extension)){
                    if($fileSize <= $maxFileSize){
                      $location = 'uploads';
                      $file->move($location,$filename);
                      $filePath = public_path($location."/".$filename);
                      $file = fopen($filePath,"r");
                      $importDataArr = array();
                      $i = 0;
                      while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                         $num = count($filedata );
                         if($i == 0){
                            $i++;
                            continue; 
                        }
                        for ($c=0; $c < $num; $c++) {
                            $importDataArr[$i][] = $filedata [$c];
                        }
                        $i++;
                    }
                    fclose($file);
                    $insertData = [];
                    foreach($importDataArr as $importData){
                        $importData[0] = isset($importData[0]) ? $importData[0] : null;//code
                        $importData[1] = isset($importData[1]) ? $importData[1] : null;//name
                        $importData[2] = isset($importData[2]) ? $importData[2] : null;//cat
                        $cat_id        = $this->getCategoryIdFromName($importData[2]);
                        $importData[3] = isset($importData[3]) ? $importData[3] : null;//price
                        $insertData[] = array(
                            "code"=>trim($importData[0]),
                            "name"=>trim($importData[1]),
                            "category_id"=>trim($cat_id),
                            "price"=>trim($importData[3])
                       );
                    }
                    // dd($insertData);
                    foreach ($insertData as $key => $value) {            
                    $checkCodeExists = Common::checkFieldExists('products','code',$value['code']);
                    if($checkCodeExists)
                        return -1;
                    }
                    ErrorLog::log("ProductsBulkInsert started",'error', __METHOD__);
                    $res = dispatch(new ProductsBulkInsert($insertData));
                    ErrorLog::log("ProductsBulkInsert ended",'error', __METHOD__);
                    return true;
                }
            }
            else{
                return false;
            }
        } catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);
            return false;
        }
    }
    public function saveProducts($request)
    {
        try {
            if (!empty($request)) {
                $pro = new Products();
                $pro->code = $request['code'];
                $pro->name = $request['name'];
                $pro->category_id = $request['category_id'];
                $pro->price = $request['price'];
                $pro->save();
            }
            return true;
        } catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);
            return false;
        }
    }
    public function deleteProduct($id)
    {
        try {
            $delete   = Products::where('id',$id)->delete();
            if($delete)
                return true;
            else 
                return false;
        } catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);
            return false;
        }
    }
    public function getCategoryIdFromName($name)
    {
        try {
            $data   = Categories::where('name',$name)->value('id');
            if($data)
                return $data;
            else 
                return 0;
        } catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);
            return 0;
        }
    }
    public function getAllCategory()
    {
        try {
            $data   = Categories::select('id','name')->get()->toArray();
            if($data)
                return json_encode($data);
            else 
                return null;
        } catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);
            return null;
        }
    }
    
}
