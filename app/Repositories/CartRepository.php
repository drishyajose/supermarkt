<?php

namespace App\Repositories;

use App\Services\ErrorLog;
use Exception;
use App\Models\Products;
use App\Models\Categories;
use App\Models\AddToCart;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Services\Common;

class CartRepository {
    
    public function __construct()
    
    {
       
    }
    public function addToCart($pid)
    {
        try {
            if($pid){
                $adcCount = AddToCart::count(); 
                $AdcExistId = AddToCart::where('product_id','=',$pid)->pluck('id')->first();
                // dd($adcCount ,$AdcExistId);
                if($adcCount == 0 || !$AdcExistId){
                    $adc = new AddToCart();
                    $adc->product_id = $pid;
                    $adc->quantity = 1;
                    $adc->save();
                }
                else{
                    $quantity = AddToCart::where('id','=',$AdcExistId)->pluck('quantity')->first();
                    // dd($quantity);
                    if($AdcExistId)
                        $update = AddToCart::where('id', $AdcExistId)->update(['quantity' => $quantity+1 ]);
                }
                return true;
            }
    } catch (Exception $e) {
        ErrorLog::log($e->getMessage(), 'error', __METHOD__);
        return false;
    }
    }
    public function viewCart()
    {
    try {
            $products = AddToCart::leftJoin('products', 'addtocart.product_id', '=', 'products.id')->select('products.code','products.name','products.price','products.id as pid','addtocart.quantity')->get()->toArray();
            return $products;
    } catch (Exception $e) {
        ErrorLog::log($e->getMessage(), 'error', __METHOD__);
        return false;
    }
    }
    public function addProductToCart($pid)
    {
        try {
            if($pid){
                $data = AddToCart::where('product_id','=',$pid)->select('id','quantity')->get()->toArray();
                if($data){
                    $update = AddToCart::where('id', $data[0]['id'])->update(['quantity' => $data[0]['quantity'] + 1 ]);
                    return true;
                }
                else{
                   return false;
                }
            }
    } catch (Exception $e) {
        ErrorLog::log($e->getMessage(), 'error', __METHOD__);
        return false;
    }
    }
    public function deleteProductFromCart($pid)
    {
        try {
            if($pid){
                $AdcExistId = AddToCart::where('product_id','=',$pid)->pluck('id')->first();
                $quantity = AddToCart::where('product_id','=',$pid)->pluck('quantity')->first();
                if($quantity == 1){
                    $delete = AddToCart::where('id', $AdcExistId)->delete();
                    return true;
                }
                else{
                    $update = AddToCart::where('id', $AdcExistId)->update(['quantity' => $quantity-1 ]);
                    return true;
                }
                return false;
            }
    } catch (Exception $e) {
        ErrorLog::log($e->getMessage(), 'error', __METHOD__);
        return false;
    }
    }
    public function placeOrder($data)
    {
        try {
            if(!empty($data)){
                $customer_name    = isset($data['customer_name']) ? $data['customer_name'] : null;
                $customer_email   = isset($data['customer_email']) ? $data['customer_email'] : null;
                $customer_mobile  = isset($data['customer_mobile']) ? $data['customer_mobile'] : null;
                $total            = isset($data['total']) ? $data['total'] : 0;
                $order_res = new Order();
                $order_res->customer_name   = $customer_name;
                $order_res->customer_email  = $customer_email;
                $order_res->customer_mobile = $customer_mobile; 
                $order_res->total           = $total; 
                $order_res->save();
                $order_id = $order_res->id;

                //order details
                $products = AddToCart::leftJoin('products', 'addtocart.product_id', '=', 'products.id')->select('addtocart.product_id','addtocart.quantity','products.price')->get()->toArray();

                foreach($products as $key => $dta){
                    $products[$key]['subtotal'] = $dta['quantity'] * $dta['price'];
                }

                $order_details = json_encode($products);
                $order_det = new OrderDetails();
                $order_det->order_id   = $order_id;
                $order_det->product_details  = $order_details;
                $order_det->save();

                AddToCart::truncate();
                return true;
            }
            return false;
    } catch (Exception $e) {
        ErrorLog::log($e->getMessage(), 'error', __METHOD__);
        return false;
    }
    }
}
