<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CartRepository;
use App\Services\ErrorLog;

class AddToCartController extends Controller
{
     public $cartRepo;

     public function __construct()
    {
        $this->cartRepo = new CartRepository();
    }
    public function addToCart(Request $request)
    {
        try{
        if($request->getMethod() =='POST') { 
            $pid = $request->has('pid')?$request->pid:null;
            if($pid){
            $add_to_cart_res =  $this->cartRepo->addToCart($pid);
            if($add_to_cart_res)
                return true;
            }else{
            return false;
            }
        }
    }catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);  
        }
    }
    public function viewCart(Request $request)
    {
        $aRequest = $request->all();
        $cart = $this->cartRepo->viewCart($aRequest);
        // dd($cart);
        $priceTotal = 0;
            foreach($cart as $model) {
                $subtotal = $model['price'] * $model['quantity'];
                $priceTotal += $subtotal;
            }
            $total = $priceTotal;
        return view('cart.cart',compact('cart'),['total'=>$total]);

    }
    public function addProductToCart(Request $request)
    {
        try{
        if($request->getMethod() =='POST') { 
            $pid = $request->has('pid')?$request->pid:null;
            if($pid){
            $add_to_cart_res =  $this->cartRepo->addProductToCart($pid);
            if($add_to_cart_res)
                return true;
            }else{
            return false;
            }
        }
    }catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);  
        }
    }
    public function deleteProductFromCart(Request $request)
    {
        try{
        if($request->getMethod() =='DELETE') { 
            $pid = $request->has('pid')?$request->pid:null;
            if($pid){
            $add_to_cart_res =  $this->cartRepo->deleteProductFromCart($pid);
            if($add_to_cart_res)
                return true;
            }else{
            return false;
            }
        }
    }catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);  
        }
    }
    public function placeOrder(Request $request)
    {
        try{
        if($request->getMethod() =='POST') {
            $aRequest = $request->all();
            // dd($aRequest);
            $place_order_res =  $this->cartRepo->placeOrder($aRequest);
            if($place_order_res == true)
                return redirect('/success');
            else
                return redirect()->back()->with('message', 'Failed ! Try later !');

        }
    }catch (Exception $e) {
            ErrorLog::log($e->getMessage(),'error', __METHOD__);  
        }
    }
}
