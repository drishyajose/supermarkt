<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductsRepository;
use App\Services\ErrorLog;



class ProductsController extends Controller
{
     public function __construct()
    {
        $this->productsRepo = new ProductsRepository();
    }

    public function view(Request $request)
    {
        $aRequest = $request->all();
        $products = $this->productsRepo->listProducts($aRequest);
        // dd($products);
        return view('products.products',compact('products'));

    }
    public function saveProducts(Request $request)
    {
        try{
        $aRequest = $request->all();
        if ($request->getMethod() == 'POST') {
            if($request->hasFile('file')){
                $file = $request->file('file');
                $products = $this->productsRepo->processCsv($file);
                if ($products === -1)
                    return redirect()->back()->with('message', 'Product Code Already Exists !');
                else if($products == true)
                    return redirect()->back()->with('message', 'Products saved successfully!');
                else
                    return redirect()->back()->with('message', 'Failed !');
        }
        else{
            // ErrorLog::log("no file  ",'error', __METHOD__);
            return redirect()->back()->with('message', 'No file uploaded!');
        }
    }
    } catch (Exception $e) {
        return redirect()->back()->with('message', $e);
        }
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $deleteProduct = $this->productsRepo->deleteProduct($id);
        return $deleteProduct;
    }
    public function getCategory()
    {
        $cat = $this->productsRepo->getAllCategory();
        // dd($cat);
        return $cat;

    }
}
