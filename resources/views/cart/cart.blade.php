<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ABC Supermarkt</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Styles -->
        
    </head>
 <div class="container">
      @if(session()->has('message'))
        <div class="alert alert-success">{{ session()->get('message') }}</div>
       @endif
    <div class="float-right"><a href="{{route('view-products')}}" ><h4>View All Products</h4></a></div>
  <h2>Checkout</h2> 
  @if(!empty($cart))
  <table class="table table-hover table-bordered">
  <thead>
    <tr>
      <th>Product Code</th>
      <th>Product Title</th>
      <th>Price</th>
      <th>Quantity</th>
      <th>Action</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($cart as $key => $product)
    <tr>
      <td>{{ $product['code']}}</td>
      <td>{{ $product['name']}}</td>
      <td>{{ $product['price']}}</td>
      <td>{{ $product['quantity']}}</td> 
      <td><button type="button" class="btn btn-primary" onclick="addOne({{$product['pid']}})">+</button> 
        <button type="button" class="btn btn-danger" onclick="deleteOne({{$product['pid']}})">-</button>
      </td>
      <td>{{ $product['quantity'] * $product['price']}}</td>
    </tr>
    @endforeach
    <tr><td colspan="5" float="right">Total</td><td>{{$total}}</td></tr>
  </tbody>
</table>

<form  action="{{ url('/order') }}" method="POST">
@csrf
  <div class="form-row mb-5">
    <div class="col md-3">
      <input type="text" class="form-control" placeholder="Customer name" name="customer_name" required>
    </div>
    <div class="col md-3">
      <input type="text" class="form-control" placeholder="Customer email" name="customer_email" required>
    </div>
    <div class="col md-3">
      <input type="text" class="form-control" placeholder="Customer mobile" name="customer_mobile" required>
      <input type="hidden" name="total" value="{{$total}}">
    </div>
    <div class="col md-2">
    <button type="submit" class="btn btn-primary">Place Order</button>
    </div>
  </div>
</form>   
@else
    <h4 class="ml-5">Checkout is Empty!</h4>
@endif
</div>
</body>
</body>
</html>

<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function addOne(pid){
            // alert(pid);
            var url = "{{ url('/add-one-product') }}";
            $.ajax({
              url: url,
              type: 'POST',
              data: { pid: pid },
              dataType: 'JSON',
              beforeSend: function (xhr) {
              },
              success: function (res) { 
                if(res == true){
                    // alert('added to cart successfully');
                location.reload();
                }else{
                    alert("Failed ! Try again !");}
              },
              error: function (error) {
                console.log('Error');
              }
           });
    }
    function deleteOne(pid){
            // alert(pid);
            var url = "{{ url('/delete-one-product') }}";
            $.ajax({
              url: url,
              type: 'DELETE',
              data: { pid: pid },
              dataType: 'JSON',
              beforeSend: function (xhr) {
              },
              success: function (res) { 
                if(res == true){
                    // alert('successful');
                location.reload();
              }else{
                    alert("Failed ! Try again !");}
              },
              error: function (error) {
                console.log('Request Error');
              }
           });
    }
</script>