<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ABC Supermarkt</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Styles -->
        
    </head>
    <body class="antialiased">
        <div class="container">
        <div class="main mb-10">
            <div class="col-md-8">
                <h3>Upload products</h3>
        </div>
            @if(session()->has('message'))
            <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
        <div style="margin-left:50px;margin-right:auto;margin-bottom:20px;">
        <form role="form" method="post" action="{{ route('save-products') }}" id="" enctype="multipart/form-data">
        <input type="file" name="file" accept=".csv">
        @csrf
        <input type="submit" value="Submit" class="btn btn-primary">
        </form>
        </div>
        </div>
        @if(!empty($products))
        <div class="col-md-8">
        <h3>Products Search</h3>
        <form method="get">
        <div class="form-row mb-5">
        <div class="col md-3">
        <input type="text" class="form-control" placeholder="Product Code" name="procode" value="<?php if(isset($_GET['procode']) && $_GET['procode'] != "") { echo $_GET['procode']; } ?>">
    </div>
    <div class="col md-3">
      <input type="text" class="form-control" placeholder="Product Name" name="proname"  value="<?php if(isset($_GET['proname']) && $_GET['proname'] != "") { echo $_GET['proname']; } ?>">
    </div>
    <div class="col md-3">
      <input type="text" class="form-control" placeholder="Price" name="proprice" value="<?php if(isset($_GET['proprice']) && $_GET['proprice'] != "") { echo $_GET['proprice']; } ?>">
    </div>
    <div class="col md-3">
      <select class="form-control" name="category" id="category">
        <option value=""  selected>All Category</option>
      </select>
    </div>
    <div class="col md-2">
    <button type="submit" class="btn btn-primary">Search</button>
    </div>
        </div>
    </form>
        </div>
        @endif

        <div class="col-md-8">
                <h3>Products List</h3>
        </div>
        <div class="row" style="padding-top: 35px;">
        	<div class="col-md-12">
                @if(!empty($products))
        		<table class="table table-hover table-bordered" style="">
                    <tr>
                        <th>SL No:</th>  
                        <th>Product Code:</th>  
                        <th>Product Name:</th>  
                        <th>Product Price:</th>  
                        <th>Product Category:</th>  
                        <th>Action</th>  
                    </tr>
                    @php $i = $products->perPage() * ($products->currentPage() -1);@endphp
        			@foreach($products as $key=>$prd)
                    @php $i++ @endphp 
                     <tr>
                        <td>{{$i}} </td>      
                        <td>{{$prd['code']}} </td>      
                        <td>{{$prd['name']}} </td>      
                        <td>{{$prd['price']}} </td>      
                        <td>{{$prd['category']['name']}} </td> 
                        <td><button type="button" class="btn btn-primary" onclick="deleteProduct({{$prd['id']}})">Delete</button></td>
                        <td><button type="button" class="btn btn-primary" onclick="addToCart({{$prd['id']}})">Add to Cart</button></td>
                    </tr>
                    @endforeach
                <tr>
                </tr>
        		</table>
                <div class="float-right"><a href="{{route('view-cart')}}" ><h4>View Cart</h4></a></div>
                @else
                <h4 class="ml-5">No products added ! Upload a csv file to add Products.</h4>
                @endif
        	</div>
            {{ $products->links() }}
        </div>
        </div>
        </div>
    </body>
</html>

<script src="{{ asset('js/products.js') }}"></script>
<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#category').on('click', function() {     
            // if ($("#category").val() == null) { 
                var url = "{{ url('/get-category') }}";
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response){  
                        console.log(response[1]['name']) ;
                        $.each(response,function(key, value) {
                            $("#category").append('<option value=' + response[key]['id'] + '>' + response[key]['name'] + '</option>');
                        });                   
                    },
                    error: function (xhr) {
                        alert("Something went wrong, please try again");
                    }
                });
            // }

        });
</script>