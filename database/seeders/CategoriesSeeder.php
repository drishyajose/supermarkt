<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $aData = [
                ['name'=>"Kitchen"],
                ['name'=>"Beauty & Care"],
                ['name'=>"Electronics"],
                ['name'=>"Books"],
                ['name'=>"Fruit & Veg"],
                ['name'=>"Meat Free"],
                ['name'=>"Meat & Poultry"],
                ['name'=>"Bakery"],
                ['name'=>"Drinks"],
                ['name'=>"Cleaning & Household"],
                ['name'=>"Pets"],
                ['name'=>"Baby Care"],
        ];

        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');

        DB::table ('categories')->truncate();

        DB::table ('categories')->insert($aData);

        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');
    }
}
