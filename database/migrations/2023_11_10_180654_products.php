<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void 
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();;
            $table->string('name');
            $table->integer('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            // $table->foreignId('category_id')->constrained(table: 'categories', indexName: 'id')->onUpdate('cascade')->onDelete('cascade');
            $table->decimal('price', 10, 2);
            $table->nullableTimestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            Schema::dropIfExists('products');
        });
    }
};
