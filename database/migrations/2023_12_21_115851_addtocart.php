<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
     public function up()
    {
        Schema::create('addtocart', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('quantity')->unsigned();
            $table->nullableTimestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     */
     public function down()
    {
        Schema::table('addtocart', function (Blueprint $table) {
            Schema::dropIfExists('addtocart');
        });
    }
};
